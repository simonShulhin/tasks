####Початок роботи з темплейтом:
1. Ініт темплейта `wiz init`
2. Фікс для Outlook в index.html: 
```bash
<!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml"
        xmlns:v="urn:schemas-microsoft-com:vml"
        xmlns:o="urn:schemas-microsoft-com:office:office">
        ...
```
```bash
        ...
    <style> a {text-decoration: none!important; }</style>
                                 
    <!--[if (mso) | (mso 16) | (gte mso 9) | (IE)]>
    <style> p{mso-line-height-rule: exactly;} a {  text-decoration: none;  } table{ border-collapse: collapse;}</style>
    <![endif]-->
                             
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    ...
```
3. Змінити шлях до блоків в `.ewizard/settings.json` з `"blocks": "node_modules/@blocks"` на `"blocks": "common/blocks-library"`
4. Створити 3 блока в `common/blocks-library` та описати їх в `common/blocks-library/blocks.json`. Поки ведеться розробка можна тимчасово! підключити блоки в темплейт (App.vue) як компоненти щоб бачити локально свої зміни (коли блоки будуть зроблені і протестовані треба буде забрати це):
``` bash
<template>
	<wiz-root>
		<MyBlockId1></MyBlockId1>
		<MyBlockId2></MyBlockId2>
		<MyBlockId3></MyBlockId3>
	</wiz-root>
</template>

<script>
	import MyBlockId1 from './common/blocks-library/my-block-id1/index.vue';
	import MyBlockId2 from './common/blocks-library/my-block-id2/index.vue';
	import MyBlockId3 from './common/blocks-library/my-block-id3/index.vue';
export default {
  name: "wiz-app",
  components: {
	  MyBlockId1,
	  MyBlockId2,
	  MyBlockId3
  }
};
```
5. Щоб зробити темплейт для розсилки потрібно накинути через візард блоки з бібліотеки в темплейт, відредагувати згідно ТЗ в візарді (якщо потрібно то руками в темплейті, але не в бібліотеці а в доданих блоках в App.vue) та зберегти. Верстка доданих блоків розвернеться в App.vue:
``` bash
<template>
	<wiz-root>
		 <wiz-block>
		 		    ...
		 </wiz-block>
		 <wiz-block>
		 		    ...
		 </wiz-block>
		 <wiz-block>
		 		    ...
		 </wiz-block>
	</wiz-root>
</template>

<script>
export default {
  name: "wiz-app"
};
```
